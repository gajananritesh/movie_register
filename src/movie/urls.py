from django.conf.urls import url
from django.views.generic import TemplateView
from . import views


urlpatterns = [
url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
url(r'^register/$', views.register_view, name='register'),
url(r'^after_reg/$', views.after_reg, name='after_reg'),
]
