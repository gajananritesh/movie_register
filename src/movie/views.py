from django.shortcuts import render, redirect
from .forms import MovieForm
from .models import Movie

def register_view(request):
    if request.method == 'POST':
        print ('working.....')
        form = MovieForm(request.POST)
        
        if form.is_valid():
            title = form.cleaned_data['title']
            year = form.cleaned_data['year']
            director = form.cleaned_data['director']
            m = Movie(title = title, year = year, director = director)
            m.save()
            return redirect('/after_reg')
        else:
            form = MovieForm()
            return render(request,'register.html', {
                'form':form,
                })
    else:
        print ('working.....')
        form = MovieForm()
        args = {'forms':form}
        return render(request, 'register.html', args)

def after_reg(request):
    return render(request,'after_reg.html')
