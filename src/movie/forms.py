from django import forms
from .models import Movie
from django.forms import ValidationError
import re
class MovieForm(forms.Form):
    title = forms.CharField(max_length=20, required='True')
    year = forms.CharField(max_length=4)
    director = forms.CharField(max_length=20)

    class Meta:
        model = Movie
        fields = ('title',
                'year',
                'director',
        )

    def clean_title(self):
        title = self.cleaned_data['title']

        if (len(title) < 3) or(len(title) > 10):
            raise ValidationError ('Enter a name between 3 to 10 chars')
        return title

    def clean_year(self):
        year = self.cleaned_data['year']
        
        matchObj = re.match(r'\D+', year)
        if matchObj:
            raise ValidationError('Enter a Number')

        if (int(year) > 1900) or(int(year) < 2020):
            raise ValidationError ('Enter a number between 1900 to 2020')
        return year

    def clean_director(self):
        director = self.cleaned_data['director']

        if (len(director) < 3) or(len(director) > 10):
            raise ValidationError ('Enter a name between 3 to 10 chars')
        return director