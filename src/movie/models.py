from django.db import models

class Movie(models.Model):
    title = models.CharField(max_length=20)
    year = models.CharField(max_length=4)
    director = models.CharField(max_length=20)

    def __str__(self):
        return self.title